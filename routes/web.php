<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [HomeController::class, "home"]);
Route::get("/register", [AuthController::class, "get_register"]);
Route::get("/welcome", [AuthController::class, "welcome"]);
Route::get("/table", [TableController::class, "get_table"]);
Route::get("/data-table", [TableController::class, "get_dataTable"]);
Route::post("/register", [AuthController::class, "post_register"]);

/* Crud Cast */
/* Create cast */
Route::get("/cast/create", [CastController::class, "get_create_cast"]);
Route::post("/cast", [CastController::class, "create_cast"]);

// Read cast
Route::get("/cast", [CastController::class, "get_casts"]);
Route::get("/cast/{id}", [CastController::class, "get_single_cast"]);

/* Update cast */
Route::get("/cast/{id}/edit", [CastController::class, "get_update_cast"]);
Route::put("/cast/{id}/edit", [CastController::class, "update_cast"]);

/* Delete cast */
Route::delete("/cast/{id}", [CastController::class, "delete_cast"]);
