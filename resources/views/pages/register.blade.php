@extends('layouts.master')
@section('title')
Halaman Pendaftaran
@endsection
@section('subtitle')
Halaman Pendaftaran
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/register" method="POST">
  @csrf
  <div>
    <label>
      <p>First name:</p>
      <input type="text" name="firstname" id="firstname" value="" />
    </label>
  </div>

  <div>
    <label>
      <p>Last name:</p>
      <input type="text" name="lastname" id="lastname" value="" />
    </label>
  </div>

  <div>
    <p>Gender:</p>
    <div>
      <label>
        <input type="radio" name="gender" id="male" value="Male" />
        Male
      </label>
    </div>

    <div>
      <label>
        <input type="radio" name="gender" id="female" value="Female" />
        Female
      </label>
    </div>

    <div>
      <label>
        <input type="radio" name="gender" id="other" value="Other" />
        Other
      </label>
    </div>
  </div>

  <div>
    <label>
      <label for="nationality">
        <p>Nationality:</p>
      </label>

      <select name="nationality" id="nationality">
        <option value="" disabled selected>
          --Please choose an option--
        </option>
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
      </select>
    </label>
  </div>

  <br />

  <div>
    <legend>Language Spoken:</legend>

    <div>
      <input type="checkbox" id="language" name="indonesia" />
      <label for="indonesia">Bahasa Indonesia</label>
    </div>

    <div>
      <input type="checkbox" id="language" name="english" />
      <label for="english">English</label>
    </div>

    <div>
      <input type="checkbox" id="language" name="other" />
      <label for="other">Other</label>
    </div>
  </div>

  <div>
    <label>
      <p>Bio:</p>
      <textarea name="" id="" cols="30" rows="10"></textarea>
    </label>
  </div>

  <br />

  <button type="submit">Sign Up</button>
</form>
@endsection

