<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function get_register()
    {
        return view('pages.register');
    }

    public function welcome()
    {
        return view('pages.welcome');
    }

    public function post_register(Request $req)
    {

        $fname = $req->firstname;
        $lname = $req->lastname;

        return view('pages.welcome', ['fname' => $fname, 'lname' => $lname]);
    }
}
