<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    // CRUD Cast

    // GET '/cast'
    public function get_casts()
    {
        $cast = DB::table("casts")->get();
        return view("casts.index", compact("cast"));
    }

    // GET '/cast/create'
    public function get_create_cast()
    {
        return view("casts.addCast");
    }

    // POST '/cast'
    public function create_cast(Request $req)
    {
        $req->validate(
            [
                "nama" => "required",
                "umur" => "required",
                "bio" => "required",
            ],
            [
                "nama.required" => "nama harus diisi",
                "umur.required" => "umur tidak boleh kosong",
                "bio.required" => "bio harus diisi",
            ]
        );
        $query = DB::table("casts")->insert([
            "nama" => $req["nama"],
            "umur" => (int) $req["umur"],
            "bio" => $req["bio"],
        ]);

        return redirect("/cast");
    }

    // GET '/cast/{cast_id}'
    public function get_single_cast($id)
    {
        $cast = DB::table("casts")
            ->where("id", $id)
            ->first();

        return view("casts.showCast", compact("cast"));
    }

    // GET '/cast/{cast_id}/edit'
    public function get_update_cast($id)
    {
        $cast = DB::table("casts")
            ->where("id", $id)
            ->first();

        return view("casts.editCast", compact("cast"));
    }

    // PUT '/cast/{cast_id}'
    public function update_cast($id, Request $req)
    {
        $req->validate(
            [
                "nama" => "required",
                "umur" => "required",
                "bio" => "required",
            ],
            [
                "nama.required" => "nama harus diisi",
                "umur.required" => "umur tidak boleh kosong",
                "bio.required" => "bio harus diisi",
            ]
        );

        $query = DB::table("casts")
            ->where("id", $id)
            ->update([
                "nama" => $req["nama"],
                "umur" => (int) $req["umur"],
                "bio" => $req["bio"],
            ]);

        return redirect("/cast");
    }

    // DELETE '/cast/{cast_id}
    public function delete_cast($id)
    {
        $query = DB::table("casts")
            ->where("id", $id)
            ->delete();
        return redirect("/cast");
    }
}
